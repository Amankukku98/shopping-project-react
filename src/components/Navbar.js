import React, { Component } from 'react'
import ShoppingCartOutlinedIcon from '@mui/icons-material/ShoppingCartOutlined';
import './style/navbar.css';
class Navbar extends Component {
    render() {
        return (
            <div>

                <div className="nav-container">
                    <h1 onClick={() => this.props.showHandler(true)}>My Shopping</h1>
                    <div className='cartImg' onClick={() => this.props.showHandler(false)}><p>Cart</p><ShoppingCartOutlinedIcon id="cart-icon" /> <span>{this.props.length}</span></div>
                </div>
            </div>
        )
    }
}

export default Navbar
