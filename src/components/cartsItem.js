import React, { Component } from 'react'
import './style/cartsItem.css';
class cartsItem extends Component {
    componentDidMount = () => {
        this.props.handlePrice();
    }

    render() {
        const { carts, removeItem, totalPrice, incrementAmount, decrementAmount } = this.props;
        return (
            <div>
                {carts.map((item) => {
                    return (
                        <>
                            <div className="cartsItem">
                                <div className='aman'>
                                    <div className='table-first'>
                                        <table >
                                            <tr>
                                                <td id='imag'><img src={item.img} alt="" /></td>
                                                <td><span>Book:</span>{item.title}</td>
                                            </tr>
                                        </table>
                                    </div>
                                    <div className='table-second'>
                                        <table className='amount-container'>
                                            <tr>
                                                <td id='minus'><button onClick={() => decrementAmount(item.id, 1)}>-</button></td>
                                                <td><h3>{item.amount}</h3></td>
                                                <td id='plus'><button onClick={() => incrementAmount(item.id, 1)}>+</button></td>
                                            </tr>
                                        </table>
                                    </div>
                                    <div className='table-third'>
                                        <table>
                                            <tr>
                                                <td><span>Price:</span>{item.price}</td>
                                                <td id='removeBtn'><button onClick={() => removeItem(item.id)}>Remove</button></td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </>
                    )
                })}
                <div className="price-container">
                    <span>Total Amount:</span><span>Rs:{totalPrice}</span>
                </div>
            </div>
        )
    }
}

export default cartsItem
